# Recipe App API Proxu

NGINX proxy app for our api

## Usage

### Environment variables

- `LISTEN_PORT`- Port tot listen on (default: `8000`)
- `APP_HOST`- Hostname for the app to forward requests to (default: `app`)
- `APP_PORT`- Port of the app to forward requests to (default: `9000`)